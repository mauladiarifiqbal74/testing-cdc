FROM quay.io/debezium/server:2.3

# Salin file JAR kustom ke dalam direktori plugins Kafka Connect
COPY custom-transform/target/InsertTimestamp-1.0-SNAPSHOT.jar /debezium/lib/
COPY custom-transform/target/InsertTimestamp-1.0-SNAPSHOT.jar /debezium/connect/libs/

# (Opsional) Jika Anda memiliki library tambahan, Anda bisa menambahkan di sini
# Buat direktori untuk menyimpan timestamp
# Salin file timestamp.txt ke dalam direktori container
COPY /timestamp/timestamp.txt /debezium/timestamp.txt

# Set environment variable untuk path file timestamp
ENV TIMESTAMP_FILE_PATH=/debezium/timestamp.txt