package com.example;

import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.connect.data.Struct;
import org.apache.kafka.connect.source.SourceRecord;
import org.apache.kafka.connect.transforms.Transformation;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

public class InsertTimestamp implements Transformation<SourceRecord> {

    private long timestamp;

    @Override
    public void configure(Map<String, ?> configs) {
        String timestampFilePath = (String) configs.get("timestamp.file.path");
        if (timestampFilePath == null) {
            throw new IllegalArgumentException("Property timestamp.file.path must be provided");
        }

        try {
            String timestampString = new String(Files.readAllBytes(Paths.get(timestampFilePath))).trim();
            this.timestamp = parseTimestamp(timestampString);
            System.out.println("Read timestamp from file: " + this.timestamp); // Log for debugging
        } catch (Exception e) {
            throw new RuntimeException("Error reading timestamp from file: " + e.getMessage(), e);
        }
    }

    private long parseTimestamp(String timestampString) {
        System.out.println("Parsing timestamp: " + timestampString); // Log parsing attempt
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = sdf.parse(timestampString);
            return date.getTime();
        } catch (Exception e) {
            System.err.println("Invalid timestamp format in file: " + timestampString);
            throw new RuntimeException("Error parsing timestamp", e);
        }
    }

    @Override
    public SourceRecord apply(SourceRecord record) {
        Object value = record.value();

        if (value instanceof Struct) {
            Struct structValue = (Struct) value;
            try {
                // Memeriksa field source dari structValue
                Struct sourceStruct = structValue.getStruct("source");
                if (sourceStruct != null) {
                    System.out.println("Source struct: " + sourceStruct.schema().fields()); // Log for debugging
                    Object snapshotObj = sourceStruct.get("snapshot");

                    // Periksa apakah snapshotObj adalah boolean atau string, dan tangani sesuai nilainya
                    boolean isSnapshot = false;
                    if (snapshotObj instanceof Boolean) {
                        isSnapshot = (Boolean) snapshotObj;
                    } else if (snapshotObj instanceof String) {
                        String snapshotStr = (String) snapshotObj;
                        isSnapshot = "true".equalsIgnoreCase(snapshotStr) || "last".equalsIgnoreCase(snapshotStr);
                    } else {
                        System.err.println("Snapshot field is not of type Boolean or String, actual type: " + (snapshotObj != null ? snapshotObj.getClass().getName() : "null"));
                    }

                    if (isSnapshot) {
                        // Create a new Struct for 'source' with the updated timestamp
                        Struct newSourceStruct = new Struct(sourceStruct.schema())
                                .put("version", sourceStruct.get("version"))
                                .put("connector", sourceStruct.get("connector"))
                                .put("name", sourceStruct.get("name"))
                                .put("ts_ms", timestamp)  // Set the timestamp here
                                .put("snapshot", sourceStruct.get("snapshot"))
                                .put("db", sourceStruct.get("db"))
                                .put("rs", sourceStruct.get("rs"))
                                .put("collection", sourceStruct.get("collection"))
                                .put("ord", sourceStruct.get("ord"));

                        System.out.println("Updated newSourceStruct: " + newSourceStruct); // Log for debugging

                        // Create a new struct for the record's value with the updated 'source'
                        Struct newValueStruct = new Struct(structValue.schema())
                                .put("source", newSourceStruct)  // Replace 'source' with the new Struct
                                .put("after", structValue.get("after"))  // Assuming 'after' field is present
                                .put("before", structValue.get("before"))  // Assuming 'before' field is present
                                .put("op", structValue.get("op"))  // Assuming 'op' field is present
                                .put("ts_ms", structValue.get("ts_ms"));  // Assuming 'ts_ms' field is present

                        System.out.println("Updated newValueStruct: " + newValueStruct); // Log for debugging

                        // Create a new SourceRecord with the updated 'value'
                        return new SourceRecord(
                                record.sourcePartition(),
                                record.sourceOffset(),
                                record.topic(),
                                record.kafkaPartition(),
                                record.keySchema(),
                                record.key(),
                                structValue.schema(),
                                newValueStruct
                        );
                    } else {
                        System.err.println("Snapshot is false, skipping timestamp update.");
                    }
                } else {
                    System.err.println("Source struct is missing or null.");
                }
            } catch (Exception e) {
                System.err.println("Error processing document: " + e.getMessage());
                e.printStackTrace(); // Print stack trace for more detailed debugging
            }
        } else {
            System.err.println("Expected record value to be of type STRUCT but found: " + (value != null ? value.getClass().getName() : "null"));
        }
        return record;
    }

    @Override
    public ConfigDef config() {
        return new ConfigDef()
            .define("timestamp.file.path", ConfigDef.Type.STRING, ConfigDef.Importance.HIGH, "Path to the timestamp file");
    }

    @Override
    public void close() {
        // No resources to close in this case
    }
}
